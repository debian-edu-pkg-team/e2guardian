Description: automatically set correct worker count on 32-bit archs
 The upstream e2guardian.conf has a message telling users they need to
 adjust the default number of http workers on 32-bit systems.  Instead
 we should just do this automatically.
 .
 Ideally this would be done at runtime by the program itself, and the
 default value could simply be commented out.
 .
 This changeset introduces setting a functional http workers value
 based on the build architecture at build time.
 .
 This patch was originally contributed to e2guardian in Debian
 and Ubuntu by Steve Langasek steve.langasek@canonical.com. See
 https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1053343
 .
 The patch has been rebased against v5.5 of e2guardian by Mike
 Gabriel mike.gabriel@das-netzwerkteam.de.
Author: Steve Langasek <steve.langasek@ubuntu.com>
Forwarded: no
Last-Update: 2023-10-02
Bug-Ubuntu: https://bugs.launchpad.net/bugs/2037958
Forwarded: https://github.com/e2guardian/e2guardian/pull/797

--- a/configs/e2guardian.conf.in
+++ b/configs/e2guardian.conf.in
@@ -1076,7 +1076,7 @@
 ### TUNING section
 ###
 
-#httpworkers = 500
+#httpworkers = @HTTP_WORKERS@
 #
 #sets the number of worker threads to use
 #
@@ -1087,7 +1087,7 @@
 # On 32-bit systems reduce this to 300 to avoid exceeding the <4GB
 # virtual memory limit and on Linux decrease the thread stack size from
 # 10MB to 2MB (ulimit -s 2048)
-# default 500
+# default @HTTP_WORKERS@
 
 #maxcontentfiltersize = 2048
 #
--- a/configure.ac
+++ b/configure.ac
@@ -541,6 +541,14 @@
 AC_DEFINE_UNQUOTED([__DATADIR],["${E2DATADIR}"],["Data directory path"])
 AC_DEFINE_UNQUOTED(__CONFFILE,"${e2sysconfdir}/${PACKAGE_NAME}.conf","Configure file path")
 
+AC_CHECK_SIZEOF([void *])
+if test $ac_cv_sizeof_void_p = 8; then
+    httpworkers=500
+else
+    httpworkers=300
+fi
+AC_SUBST([HTTP_WORKERS], [$httpworkers])
+
 AC_CONFIG_FILES([Makefile
 data/Makefile
 data/languages/Makefile
--- a/src/OptionContainer.cpp
+++ b/src/OptionContainer.cpp
@@ -848,7 +848,7 @@
         proc.daemon_group_name = __PROXYGROUP;
     }
 
-    proc.http_workers = cr.findoptionIWithDefault("httpworkers", 20, 20000, 500);
+    proc.http_workers = cr.findoptionIWithDefault("httpworkers", 20, 20000, __HTTPWORKERS);
 
     return true;
 
--- a/src/Makefile.am
+++ b/src/Makefile.am
@@ -62,6 +62,7 @@
 			-D__PROXYUSER='"$(E2PROXYUSER)"' \
 			-D__PROXYGROUP='"$(E2PROXYGROUP)"' \
 			-D__CONFDIR='"$(E2CONFDIR)"' \
+			-D__HTTPWORKERS='$(HTTP_WORKERS)' \
 			$(AM_CPPFLAGS)
 e2guardian_SOURCES = String.cpp String.hpp \
                        FDTunnel.cpp FDTunnel.hpp \
